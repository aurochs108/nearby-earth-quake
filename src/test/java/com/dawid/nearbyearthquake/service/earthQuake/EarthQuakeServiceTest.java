package com.dawid.nearbyearthquake.service.earthQuake;

import com.dawid.nearbyearthquake.model.earthQuake.EarthQuakeDTO;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class EarthQuakeServiceTest {
    private static EarthQuakeService earthQuakeService;

    @BeforeAll
    private static void beforeAll() {
        earthQuakeService = new EarthQuakeService(new RestTemplate());
    }

    @ParameterizedTest
    @CsvSource({
            "180,      0",
            "0,   0",
            "1,    -1",
            "0,    -180",
            "40,    -40",
            "40,    0",
            "60,    60",
    })
    public void shouldGet10NearestEarthQuakesAsListReturnValidResult(double lon, double lat) throws Exception {
        List<EarthQuakeDTO> nearbyEarthQuakes = earthQuakeService
                .get10NearbyEarthQuakes(lon, lon);

        List<String> listOfNearbyEarthQuakesData = new ArrayList<>();
        nearbyEarthQuakes.forEach(earthQuakeDTO -> listOfNearbyEarthQuakesData
                .add(earthQuakeDTO.getNearbyEarthQuakeData().substring(5)));

        assertThat(nearbyEarthQuakes).isNotEmpty().hasSize(10);
        assertThat(listOfNearbyEarthQuakesData).isNotEmpty().hasSize(10).doesNotHaveDuplicates();
    }

    @Test
    public void shouldIsLatLonValidReturnValidResult() {
        boolean latLonValid1 = earthQuakeService.isLatLonValid(180.0, 180.0);
        boolean latLonValid2 = earthQuakeService.isLatLonValid(-180.0, -180.0);
        boolean latLonValid3 = earthQuakeService.isLatLonValid(-180.0, 180.0);
        boolean latLonValid4 = earthQuakeService.isLatLonValid(180.0, -180.0);
        boolean latLonValid5 = earthQuakeService.isLatLonValid(0, 0);
        boolean latLonValid6 = earthQuakeService.isLatLonValid(-0.0, -0.0);

        assertThat(latLonValid1).isEqualTo(true);
        assertThat(latLonValid2).isEqualTo(true);
        assertThat(latLonValid3).isEqualTo(true);
        assertThat(latLonValid4).isEqualTo(true);
        assertThat(latLonValid5).isEqualTo(true);
        assertThat(latLonValid6).isEqualTo(true);
    }

    @Test
    public void shouldIsLatLonValidThrowErrorWhenLatLonIsNotValid() {
        assertThatThrownBy(() -> earthQuakeService.isLatLonValid(-180.1, 0))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("Longitude or Latitude is not in valid range(-180; 180)");

        assertThatThrownBy(() -> earthQuakeService.isLatLonValid(-0, 180.1))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("Longitude or Latitude is not in valid range(-180; 180)");

        assertThatThrownBy(() -> earthQuakeService.isLatLonValid(180.1, -0))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("Longitude or Latitude is not in valid range(-180; 180)");

        assertThatThrownBy(() -> earthQuakeService.isLatLonValid(-0, -180.1))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("Longitude or Latitude is not in valid range(-180; 180)");
    }

}