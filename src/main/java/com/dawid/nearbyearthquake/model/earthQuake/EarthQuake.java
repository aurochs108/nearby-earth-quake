package com.dawid.nearbyearthquake.model.earthQuake;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class EarthQuake {
    private String title;
    private int distanceBetweenGivenLocation;
    private double lat;
    private double lon;

}
