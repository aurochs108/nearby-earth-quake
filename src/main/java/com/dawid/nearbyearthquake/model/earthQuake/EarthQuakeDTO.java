package com.dawid.nearbyearthquake.model.earthQuake;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class EarthQuakeDTO {
    private String nearbyEarthQuakeData;

    public EarthQuakeDTO(String title, int distanceBetweenLocation) {
        this.nearbyEarthQuakeData = title + " || " + distanceBetweenLocation;
    }


}
