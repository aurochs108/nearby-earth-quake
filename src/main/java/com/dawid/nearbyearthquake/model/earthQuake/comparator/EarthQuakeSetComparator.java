package com.dawid.nearbyearthquake.model.earthQuake.comparator;

import com.dawid.nearbyearthquake.model.earthQuake.EarthQuake;

import java.util.Comparator;

public class EarthQuakeSetComparator implements Comparator<EarthQuake> {

    @Override
    public int compare(EarthQuake eq1, EarthQuake eq2) {
        return eq1.getDistanceBetweenGivenLocation() - eq2.getDistanceBetweenGivenLocation();
    }
}
