package com.dawid.nearbyearthquake.controller;

import com.dawid.nearbyearthquake.model.earthQuake.EarthQuakeDTO;
import com.dawid.nearbyearthquake.service.earthQuake.EarthQuakeService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController()
@RequestMapping("/earthQuake")
public class EarthQuakeController {
    private EarthQuakeService earthQuakeService;

    public EarthQuakeController(EarthQuakeService earthQuakeService) {
        this.earthQuakeService = earthQuakeService;
    }

    @GetMapping(value = "/{lat}/{lon}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<EarthQuakeDTO>> get10ClosestEarthQuakesFromLast30Days(
            @PathVariable double lat, @PathVariable double lon) throws Exception {
        earthQuakeService.isLatLonValid(lat, lon);
        List<EarthQuakeDTO> nearbyEarthQuakeData = earthQuakeService.get10NearbyEarthQuakes(lat, lon);
        return ResponseEntity.ok()
                .body(nearbyEarthQuakeData);
    }
}
