package com.dawid.nearbyearthquake.service.earthQuake;

import com.dawid.nearbyearthquake.model.earthQuake.EarthQuake;
import com.dawid.nearbyearthquake.model.earthQuake.EarthQuakeDTO;
import com.dawid.nearbyearthquake.model.earthQuake.comparator.EarthQuakeSetComparator;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@Service
public class EarthQuakeService {
    private static final String LAST_30_DAYS_EARTHQUAKES_URL
            = "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_month.geojson";
    private static final double AVERAGE_RADIUS_OF_EARTH = 6371;
    private RestTemplate restTemplate;

    public EarthQuakeService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public List<EarthQuakeDTO> get10NearbyEarthQuakes(double longitude, double latitude) {
        String allEarthQuakeDataFrom30DaysJson = getAllEarthQuakeDataFrom30Days();
        Set<EarthQuake> earthQuakes = parseEarthQuakeJsonToSetOfEarthQuakes(allEarthQuakeDataFrom30DaysJson,
                longitude, latitude);
        return get10NearestEarthQuakesAsList(earthQuakes);
    }

    private List<EarthQuakeDTO> get10NearestEarthQuakesAsList(Set<EarthQuake> earthQuakes) {
        List<EarthQuakeDTO> tenNearestEarthQuakes = new ArrayList<>();
        int listCounter = 0;
        for (EarthQuake earthQuake : earthQuakes) {
            if (listCounter < 10) {
                if (isEarthQuakeAtTheSameLatLonOnTheList(listCounter, earthQuakes, earthQuake)) {
                    continue;
                }
                tenNearestEarthQuakes
                        .add(new EarthQuakeDTO(earthQuake.getTitle(), earthQuake.getDistanceBetweenGivenLocation()));
                listCounter++;
            } else {
                return tenNearestEarthQuakes;
            }
        }
        return Collections.emptyList();
    }

    private boolean isEarthQuakeAtTheSameLatLonOnTheList(int actualLoopCounter, Set<EarthQuake> earthQuakes,
                                                         EarthQuake actualEarthQuake) {
        if (actualLoopCounter == 0) {
            return false;
        }
        int i = 0;
        for (EarthQuake earthQuake : earthQuakes) {
            if (earthQuake.getLat() == actualEarthQuake.getLat() && earthQuake.getLon() == actualEarthQuake.getLon()) {
                return true;
            }
            if (i == actualLoopCounter - 1) {
                return false;
            }
            i++;
        }
        return true;
    }

    private String getAllEarthQuakeDataFrom30Days() {
        return restTemplate.getForObject(LAST_30_DAYS_EARTHQUAKES_URL, String.class);
    }

    private Set<EarthQuake> parseEarthQuakeJsonToSetOfEarthQuakes(String earthQuakeJson,
                                                                  double longitudeFromRequest,
                                                                  double latitudeFromRequest) {
        DocumentContext parse = JsonPath.parse(earthQuakeJson);

        JsonPath titlePath = JsonPath.compile("$.features[*].properties.title");
        List<String> listOfLocations = parse.read(titlePath);

        JsonPath longitudePath = JsonPath.compile("$.features[*].geometry.coordinates[0]");
        List<Double> listOfLongitude = parse.read(longitudePath);

        JsonPath latitudePath = JsonPath.compile("$.features[*].geometry.coordinates[1]");
        List<Double> listOfLatitude = parse.read(latitudePath);

        return createEarthQuakesSet(listOfLocations, listOfLatitude, listOfLongitude,
                latitudeFromRequest, longitudeFromRequest);
    }

    private Set<EarthQuake> createEarthQuakesSet(List<String> listOfLocations, List<Double> listOfLatitude,
                                                List<Double> listOfLongitude, double latitudeFromRequest,
                                                double longitudeFromRequest) {
        Set<EarthQuake> earthQuakeSet;
        earthQuakeSet = new TreeSet<>(new EarthQuakeSetComparator());
        for (int i = 0; i < listOfLocations.size(); i++) {
            int distance = calculateDistance(listOfLatitude.get(i), listOfLongitude.get(i),
                    latitudeFromRequest, longitudeFromRequest);

            EarthQuake earthQuake = EarthQuake.builder()
                    .distanceBetweenGivenLocation(distance)
                    .title(listOfLocations.get(i))
                    .lat(listOfLatitude.get(i))
                    .lon(listOfLongitude.get(i))
                    .build();

            earthQuakeSet.add(earthQuake);
        }
        return earthQuakeSet;
    }


    private int calculateDistance(double userLat, double userLng, double venueLat, double venueLng) {
        double latDistance = Math.toRadians(userLat - venueLat);
        double lngDistance = Math.toRadians(userLng - venueLng);

        double a = (Math.sin(latDistance / 2) * Math.sin(latDistance / 2)) +
                (Math.cos(Math.toRadians(userLat))) *
                        (Math.cos(Math.toRadians(venueLat))) *
                        (Math.sin(lngDistance / 2)) *
                        (Math.sin(lngDistance / 2));

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return (int) (Math.round(AVERAGE_RADIUS_OF_EARTH * c));
    }

    public boolean isLatLonValid(double lat, double lon) {
        if (lat < -180 || lat > 180 || lon < -180 || lon > 180) {
            throw new IllegalArgumentException("Longitude or Latitude is not in valid range(-180; 180)");
        }
        return true;
    }


}
