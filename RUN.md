1. To run this program run : <code>NearbyEarthQuakeService.main(String[] args)</code>
2. Next find line in Run window :<code>2020-03-09 22:49:05.024  INFO 9636 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat initialized with port(s): 8080 (http)</code> and find out the localhost port (in this example <code>8080</code>)
3. Enter address in Postman or your browser: (enter your port in 8080 place) <code>http://localhost:8080/earthQuake/lon/lat</code> enter you longitude and latitude values to lon and lat places
4. Read values as json response